﻿M.AutoInit();

$(document).ready(function () {
    $("#resultConten").hide();

    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd'        
    });
});

$("#searchFlights").click(function () {
    $("#resultConten").show();
    $("#Table>tbody").empty();

    var $origin = $("#origin").val();
    var $arrival = $("#destination").val();
    var $date = $("#date").val(); 

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: 'http://testapi.vivaair.com/otatest/api/values',
        crossDomain: true,
        data: {
            "Origin": $origin, "Destination": $arrival, "From": $date
        },
        dataType: 'json',
        success: function (data) {
            var flights = JSON.parse(data);
            $.each(flights, function (key, item) {
                var getContent = "?DepartureDate=" + item.DepartureDate;
                getContent += "&DepartureStation=" + item.DepartureStation;
                getContent += "&ArrivalStation=" + item.ArrivalStation;
                getContent += "&FlightNumber=" + item.FlightNumber;
                getContent += "&Price=" + item.Price;
                getContent += "&Currency=" + item.Currency;

                var row =
                    "<tr>" +
                    "<td>" + item.DepartureStation + "</td>" +
                    "<td>" + item.ArrivalStation + "</td>" +
                    "<td>" + item.DepartureDate + "</td>" +
                    "<td> <a href='../../Flights/Create" + getContent + "'>Continuar</a> </td>" +
                    "</tr>";
                $("#Table>tbody").append(row);

                console.log("Origen: " + item.DepartureStation
                    + "\nDestino: " + item.ArrivalStation
                    + "\nFecha: " + item.DepartureDate
                    + "\nNumero de vuelo: " + item.FlightNumber)
            });

            console.log('Datos Consultados !!!\n' + data);
        },
        error: function () {
            console.log("Error: " + arguments);
        }
    });
});