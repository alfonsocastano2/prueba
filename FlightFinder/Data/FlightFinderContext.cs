﻿using Microsoft.EntityFrameworkCore;
using FlightFinder.Models;

namespace FlightFinder.Data
{
    /// <summary>
    ///     Context class for DB
    /// </summary>
    public class FlightFinderContext : DbContext
    {
        /// <summary>
        ///     Constructor of context class
        /// </summary>
        /// <param name="options"></param>
        public FlightFinderContext (DbContextOptions<FlightFinderContext> options)
            : base(options)
        {

        }

        /// <summary>
        ///     Models mapping for Database tables
        /// </summary>
        public DbSet<Flight> Flight { get; set; }

        public DbSet<Transport> Transport { get; set; }
    }
}
