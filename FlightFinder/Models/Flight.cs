﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FlightFinder.Models
{
    public class Flight
    {
        public int Id { get; set; }

        [Required]
        public string DepartureStation { get; set; }

        [Required]
        public string ArrivalStation { get; set; }

        [Required]
        public DateTime DepartureDate { get; set; }

        public Transport Transport { get; set; }

        [Required]
        public Decimal Price { get; set; }

        [Required]
        public string Currency { get; set; }
    }
}
