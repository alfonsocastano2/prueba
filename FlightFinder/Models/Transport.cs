﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FlightFinder.Models
{
    public class Transport
    {
        /// <summary>
        ///     ID attribute of Transport class
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        ///     FlightNumber attribute of Transport class
        /// </summary>
        [Required]
        public string FlightNumber { get; set; }
    }
}
