# prueba

Prueba de ingreso Newshore.

- 	Se crea un proyecto de .Net core C# con nombre FlightFinder

-	-	Se crea un modelo para cada clase requerida

	-	Flight
	-	Transport

-	Para interacturar con la base de datos, instalo desde el Administrador de Paquetes Nuget, el proveedor de SQL Server de 	Entity Framework Core con el siguiente comando

	-	Install-Package Microsoft.EntityFrameworkCore.SqlServer

-	Se crea un contexto en una nueva carpeta que llame Data y este contexto se registra en el archivo startup.cs en el 			método
	
	-	ConfigureServices 

-	Se guarda la cadena de conexión en el archivo de configuración de la apliación

	-	appsettings.json

-	Se crea una migración inicial para crear la base de datos

	-	Add-Migration FlightFinderDBInitialCreate

-	Se realiza la actualización de la base de datos para ejecutar la migración creada 

	-	Update-Database

		-	Esta actualización ejecuta la ultima migración craeda.

-	Para consumir la API se usa

	-	AJAX Jquery

-	Para el datapicker del formulario de busqueda se usa:

	-	Materialize.js

-	Guarda la información del vuelo

	-	Para guardar la información del vuelo, se carga en el formulario de crear vuelo, toda la información
		esta se envía por medio de get y se captura en la vista para cargar los campos del formulario con esta
		información. Dar clic en el boton de crear y se guardará la información en la base de datos	

-	No Guarda el número de vuelo

-	No se realiza Logging en aplicación

-	No se realiza Inyección de dependencias (Windsor, Ninject)

-	No se realiza Test unitarios y de integración (MSTest, NUnit)

URL of pull request

	-	"https://gitlab.com/alfonsocastano2/prueba/-/merge_requests/1"